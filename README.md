# phylogenie machines

The machines installation recipe for the pasteur phylogny course .
The recipes are based on [ansible](http://docs.ansible.com/).
It also use *gensoft* yum repository then it will work for hosts operating *CentOS6*.

The installed packages are:
- beast
- beagle
- bmge
- clustaO
- clustalW
- fastme
- FigTree
- IQ-TREE
- goalign
- gotree
- jmodeltest2
- lsd
- mafft
- mrbayes
- msaprobs
- muscle
- palm
- raxml
- raxml-ng
- seaview
- tracer
- tempest
- pastml

and
- the libreoffice suite
- vim
- emacs
- xemacs
- nano
- gedit
- firefox
- wget and curl


# how to deploy a new machine

## clone the project
    
    `git clone git@gitlab.pasteur.fr:bneron/phylogeny-machines.git`
    or
    `https://gitlab.pasteur.fr/bneron/phylogeny-machines.git`
    
## install ansible

The framework used to install the host is ansible.
So if you are not familiar with it, please check the [ansible documentation](http://docs.ansible.com/ansible/latest/index.html).    
Ansible needs to be installed on the local host, 
python >= 2.7 or >=3.5 and ssh must be installed on the remote hosts

## play the ansible playbook

Tell to ansible the hosts to deploy

```
vim Inventory/hosts
[phylogeny]
157.99.191.140
```

then run it

```
ansible-playbook --limit phylogeny -u formateur --ask-pass --become  phylogeny.yml
```

If you want to play only one or a set of playbook see available tags in practices.yml then add option `-t <tag>`
then run

```
ansible-playbook  --limit phylogeny -u formateur --ask-pass --become -t editors phylogeny.yml
``` 

# how to install a new soft

# from a gensoft package

If the software is packaged in gensoft repository (http://mirrors.web.pasteur.fr/gensoft/)
just add an item in the gensoft_packs dict located Inventory/group_vars/all file.

* **name**: is the name of the gensoft package.
* **vers**: is the package version.

# from sources

You have to write a new role and add it in the phylogeny.yml playbook
and play the role

# add an entry in the phylogeny menu

If the entry you want to add is installed via gensoft (see above),
you just have to add an item in the gensoft_menu dict located Inventory/group_vars/all files 
But if your soft is not installed via gensoft you have to write a rule to add it in menu 
by your self.


* **pack_name**: is the name of the gensoft package.
* **bin_name**: is the name of the binary to launch.
* **vers**: is the package version.
* **term**: is true if the binary need a terminal to be launch. false otherwise.

then play the tag menu
```
ansible-playbook  -t menu phylogeny.yml
```

if it's a new gensoft package run the entire role gensoft:

```
ansible-playbook  -t gensoft phylogeny.yml
```